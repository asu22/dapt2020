# DAPT2020

This dataset, [DAPT 2020](https://gitlab.com/asu22/dapt2020), was created with network traffic collected over 5 days, where in each day can be considered as analogous to 3 months in real-world scenario. 
This dataset has been created to give researchers the ability to understand the anomalies, relations ship between various attack vectors and to find out any hidden correlations that help in detecting an APT attack in its early stages. Stages in our dataset are in alignment with the stages discussed by authors of [1]. 

CONTACT: Please reach out to smyneni2@asu.edu or achaud16@asu.edu if you have any questions in regards to the dataset. <br />
Link to publication at Springer: https://link.springer.com/chapter/10.1007%2F978-3-030-59621-7_8 <br />
**CITATION**: You may cite this dataset as following: <br />
    **Sowmya Myneni, Ankur Chowdhary, Abdulhakim Sabur, Sailik Sengupta, Garima Agrawal, Dijiang Huang, and Myong Kang. <br />
    "Dapt 2020-constructing a benchmark dataset for advanced persistent threats." <br />
    In International Workshop on Deployable Machine Learning for Security Defense, pp. 138-163. Springer, Cham, 2020.<br />** 

[[_TOC_]]

## Details on Environment

<img style="text-align:center" src="system_setup.png" width="624">

**Public VM (192.168.3.29)**

The VM (Public VM, Private VM, and Log Server) are connected with each other over internal network (192.168.3.0/24). Each VM has multiple docker services exposed on the physical ports of the host VM. For instance “dvwa” is a docker instance of Damn Vulnerable Web Application. The application is running as a container on Public VM. The application has a web server, database server, etc. If the IP of the docker instance is “172.16.0.2” and service runs on port “80”, i.e., 172.16.0.2:80, we map it to an open port of Public VM, {172.16.0.2:80 → 192.168.3.29:9000}. 

The docker images were created using publically available vulnerable services used in academia and industry for penetration testing. We utilized Damn Vulnerable Web App (DVWA), Bad Store, Meta exploitable, and OWASP mutillidae containers. Details of VM preparation are provided in User Manual below. 

Additionally we utilize “TCP Dump” utility and “Snort IDS” to capture the live traffic and attack signatures of known attacks, e.g., SYN Flood, Illegal Access Attempt, etc. We use “filebeat” agent to capture the host logs - “Audit Logs”, “IDS Logs”, “Access Logs”, etc. and send them to Log Server VM over internal IP address of the Elastic Search (192.168.3.31:9200). Details on filebeat setup and configuration will be discussed in User Manual. 

**Private VM (192.168.3.30)**

Private VM has a list of services with known vulnerabilities, however, the VM (192.168.3.30) is connected to only internal network. This VM has list of services relatively harder to exploit, and require more in-depth application security and service protocol understanding by the penetration tester/ attacker. We utilized VulnHub [1], a collection of Open Source vulnerable docker environments for composing docker containers on this VM. Current set up hosts vulnerable services: wordpress website, FTP 2.3.4, DNS. MySQL, vulnerable Web application, Nessus, and SAMBA service, exposed on the ports of VM as shown in Figure 2 above. A prerequisite on this machine is “Docker Compose” apart from docker application. We use “filebeat” agent to capture the host logs - “Audit Logs”, “Access Logs”, etc. and send them to Log Server VM similar to Public VM. 

**Log Server VM (192.168.3.31)**

This is the log server which consists of ELK: Elastic Search, Logstash, and Kibana (Dashboard) for log collection, filtering and correlation. Each service runs on a separate docker container as shown in Figure 2. The ELK set up tutorial from link [2]  was followed for this. The services are exposed on the public ports on host machine. User can also utilize Docker APIs for interaction with the containers. API network in Red color can be used for accessing the ELK APIs. For instance to check “json” output from ElasticSearch, request below can be used. 

$ curl 'http://{IP of Log Server VM}:9200/?pretty'


**Gateway Router VM (206.207.50.35, 192.168.3.1)**

Only Gateway Router VM is connected to the external/public network. For the services we want to expose on the external network (206.207.50.35), we use NAT rules provided by rc.firewall to expose those services on external network. Mapping details and NAT rules used have been discussed as a part of “User Manual” section below.

**Docker Network**

The docker0 on each machine acts as a bridge to connect to the interfaces of individual containers. The network in green shows Docker API network, which can be used on host VM to run docker commands directly on container instances.


## Background Details on Services

**Public VM Service Description**

*Damn Vulnerable Web Application (DVWA)*  is a PHP/MySQL web application that is damn vulnerable. Its main goals are to be an aid for security professionals to test their skills and tools in a legal environment, help web developers better understand the processes of securing web applications and aid teachers/students to teach/learn web application security in a class room environment.  

BadStore.net presents a typical three-tier web storefront application. This self-contained application was built from the ground up with typical security mistakes to serve as a platform for demonstration, security training, evaluation, and testing purposes.  

*OWASP Mutillidae II* is a free, open source, deliberately vulnerable web-application providing a target for web-security enthusiast. Mutillidae can be installed on Linux and Windows using LAMP, WAMP, and XAMMP. It is pre-installed on SamuraiWTF and OWASP BWA. The existing version can be updated on these platforms.  

Metasploitable is a VM that is built from the ground up with a large amount of security vulnerabilities. It is intended to be used as a target for testing exploits with metasploit.  

CICFlowMeter [10],[11] is a network traffic flow generator which has been written in Java and offers more flexibility in terms of choosing the features you want to calculate, adding new ones, and having a better control of the duration of the flow timeout. It generates Bidirectional Flows (Biflow), where the first packet determines the forward (source to destination) and backward (destination to source) directions, hence the 83 statistical features such as Duration, Number of packets, Number of bytes, Length of packets, etc. are also calculated separately in the forward and reverse direction.   

Snort is an open source network intrusion prevention system, capable of performing real-time traffic analysis and packet logging on IP networks. It can perform protocol analysis, content searching/matching, and can be used to detect a variety of attacks and probes, such as buffer overflows, stealth port scans, CGI attacks, SMB probes, OS fingerprinting attempts, and much more.  



**Private VM Service Description**

*Nexus Repository Manager 3* - missing access controls and Remote Code Execution vulnerabilities [6].
*Mysql* consists of CVE-2012-2122. Details on how to exploit vulnerability is present in README file inside vulnhub source directory.
*Wordpress 4.6*  - Remote code execution vulnerability CVE-2016-10033.
*Samba*: Consists of CVE-2017-7494, can be exploited using metasploitable.
*FTP 2.3.4* consists of Backdoor Command Execution Vulnerabilities [8].
*DNS* service consists of BIND zone-transfer vulnerability. Details on how to exploit vulnerability is present in README file inside vulnhub source directory.
*Web-app Django < 2.0.8* consists of Open Redirect Vulnerability, CVE-2018-14574.




**Log Server VM Service Description**

*ELK Stack:* "ELK" is an acronym for three open source projects: Elasticsearch, Logstash, and Kibana. Elasticsearch is a search and analytics engine. Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a "stash" like Elasticsearch. Kibana lets users visualize data with charts and graphs in Elasticsearch. 

*Filebeat:* is an agent which can be used for collecting logs from individual hosts and forwarding to ELK server. It comes with internal modules (auditd, Apache, NGINX, System, MySQL, and more) that simplify the collection, parsing, and visualization of common log formats down to a single command. 

They achieve this by combining automatic default paths based on your operating system, with Elasticsearch Ingest Node pipeline definitions, and with Kibana dashboards. Plus, a few Filebeat modules ship with preconfigured machine learning jobs.     

## User Manual: Build Your Own Environment (BYOE)

The initial requirement is presence of docker on all machines. The assumption for this part is that you have Ubuntu 16 or 18.04 (64-bit) with at-least 80 Gb Hard Disk and 8 Gb RAM. The physical server should be large enough to support the creation of multiple VMs. There should be “vbox” or “VMWare” virtualization software installed on physical server, preferably with web access. We used “phpvirtualbox” on physical server.

### Pre-Requisites

 - 64-bit Ubuntu
 - Git
 - Vbox/VMware on physical server (phpvirtualbox for remote access)
 - Physical server should support virtualization

Copy Paste the contents of the docker.sh script in this git repo, on to each VM. Script can be downloaded from https://gitlab.com/asu22/dapt2020/-/blob/main/docker.sh.

 > chmod a+x docker.sh
 > ./docker.sh

This will set up the docker environment on your machine

### Preparation for Public VM

Pull and run metasploitable container using command below
 > sudo docker pull netsecframework/metasploitable:vulnstacktwo
 > sudo docker run -d -t --name=metaexp netsecframework/metasploitable:vulnstacktwo

Follow the link [3] to set up and run “dvwa” container.
Follow link [4] to set up and run “OWASP mutillidae” container.
Follow link [5] to set up and run “badstore” container.
Install java-8 or Openjdk in order to run cloned CICFlow meter jar [10].
Configure filebeat using tutorial [9].
Run command below to check if “filebeat” works correctly

 > filebeat test config
 > filebeat test output

### Preparation for Private VM

For setting up FTP service follow link [7] below. For rest of the services, follow vulnhub set up.
Follow the tutorial from vulnhub [2] to clone this git repo and install “docker-compose”. Change directory to the home directory of each service individually. 
Check the README.md file for each vulnerable service.
Run the commands below for each service separately.

 - Compile environment:
 > docker-compose build

 - Run environment
 > docker-compose up -d

 - Configure filebeat using tutorial [9]. Run the command below to check if “filebeat” works correctly
 > filebeat test config

 > filebeat test output

### Preparation of Log Server VM

 - Configure ELK stack using tutorial [2].
 - Configure filebeat using tutorial [9].
 - Run command below to check if “filebeat” works correctly.

 > filebeat test config
 
 > filebeat test output



### Preparation of Gateway Router VM

rc.firewall

PUBLIC_IFACE="eth0"
PUBLIC_IP="206.207.50.35"
LAN_MGMT_GW="192.168.3.1"
LAN_MGMT_IP_RANGE="192.168.3.0/24"
LAN_MGMT_IFACE="eth1"
LAN_DATA_IP_RANGE="10.3.0.0/16"
LAN_DATA_GW="10.3.0.1"
LAN_DATA_IFACE="eth2"
IPTABLES="/sbin/iptables"

#### Internet Access through Gateway

$IPTABLES -I INPUT -s 0.0.0.0/0.0.0.0 -d 192.168.101.3/32 -j DROP

$IPTABLES -t nat -A POSTROUTING -o $PUBLIC_IFACE -j MASQUERADE

$IPTABLES -A FORWARD -i $PUBLIC_IFACE -o $LAN_MGMT_IFACE -m state --state RELATED,ESTABLISHED -j ACCEPT

$IPTABLES -A FORWARD -i $PUBLIC_IFACE -o $LAN_MGMT_IFACE -j ACCEPT

#### Port Forwarding

$IPTABLES -t nat -A PREROUTING -p tcp -d $PUBLIC_IP --dport 9000 -j DNAT --to-destination 192.168.3.29:9000
$IPTABLES -t nat -A POSTROUTING -s 192.168.3.29 -p tcp --sport 9000 -j SNAT --to-source $LAN_MGMT_GW

$IPTABLES -t nat -A PREROUTING -p tcp -d $PUBLIC_IP --dport 9001 -j DNAT --to-destination 192.168.3.29:9001
$IPTABLES -t nat -A POSTROUTING -s 192.168.3.29 -p tcp --sport 9001 -j SNAT --to-source $LAN_MGMT_GW

$IPTABLES -t nat -A PREROUTING -p tcp -d $PUBLIC_IP --dport 9002 -j DNAT --to-destination 192.168.3.29:9002
$IPTABLES -t nat -A POSTROUTING -s 192.168.3.29 -p tcp --sport 9002 -j SNAT --to-source $LAN_MGMT_GW

$IPTABLES -t nat -A PREROUTING -p tcp -d $PUBLIC_IP --dport 9003 -j DNAT --to-destination 192.168.3.29:80
$IPTABLES -t nat -A POSTROUTING -s 192.168.3.29 -p tcp --sport 80 -j SNAT --to-source $LAN_MGMT_GW
~


### Sending PCAP Data to Elastic Search

Record the pcap data using “tcpdump” 
 > tcpdump -i eth0 -s 65535 -w packets.pcap

Convert packets to JSON Format
 > tshark -i eth0 -T ek > packets.json

Create an Index

 > curl -XPOST "http://192.168.3.31:9200/packets/_doc/_bulk?pretty" -H 'Content-Type: application/json' -d'
{"index" : {"_index": "packets-2018-04-13", "_type": "pcap_file"}}
{"timestamp" : "1493208068301", "layers" : {"frame": {"frame_frame_encap_type": "1","frame_frame_time": "Apr 26, 2017 14:01:08.301052000 CEST","frame_frame_offset_shift": "0.000000000","frame_frame_time_epoch": "1493208068.301052000","frame_frame_time_delta": "0.000007000","frame_frame_time_delta_displayed": "0.000007000","frame_frame_time_relative": "47.264793000","frame_frame_number": "5000","frame_frame_len": "165","frame_frame_cap_len": "165","frame_frame_marked": "0","frame_frame_ignored": "0","frame_frame_protocols": "eth:ethertype:ip:udp:mndp"},"eth": {"eth_eth_dst": "ff:ff:ff:ff:ff:ff","eth_dst_eth_dst_resolved": "Broadcast","eth_dst_eth_addr": "ff:ff:ff:ff:ff:ff","eth_dst_eth_addr_resolved": "Broadcast","eth_dst_eth_lg": "1","eth_dst_eth_ig": "1","eth_eth_src": "00:50:56:9e:05:1b","eth_src_eth_src_resolved": "Vmware_9e:05:1b","eth_src_eth_addr": "00:50:56:9e:05:1b","eth_src_eth_addr_resolved": "Vmware_9e:05:1b","eth_src_eth_lg": "0","eth_src_eth_ig": "0","eth_eth_type": "0x00000800"},"ip": {"ip_ip_version": "4","ip_ip_hdr_len": "20","ip_ip_dsfield": "0x00000000","ip_dsfield_ip_dsfield_dscp": "0","ip_dsfield_ip_dsfield_ecn": "0","ip_ip_len": "151","ip_ip_id": "0x00000000","ip_ip_flags": "0x00004000","ip_flags_ip_flags_rb": "0","ip_flags_ip_flags_df": "1","ip_flags_ip_flags_mf": "0","ip_flags_ip_frag_offset": "0","ip_ip_ttl": "64","ip_ip_proto": "17","ip_ip_checksum": "0x00009b7d","ip_ip_checksum_status": "2","ip_ip_src": "151.216.7.1","ip_ip_addr": ["151.216.7.1","255.255.255.255"],"ip_ip_src_host": "151.216.7.1","ip_ip_host": ["151.216.7.1","255.255.255.255"],"ip_ip_dst": "255.255.255.255","ip_ip_dst_host": "255.255.255.255"},"udp": {"udp_udp_srcport": "60474","udp_udp_dstport": "5678","udp_udp_port": ["60474","5678"],"udp_udp_length": "131","udp_udp_checksum": "0x00005f36","udp_checksum_udp_checksum_calculated": "0x00005f36","udp_udp_checksum_status": "1","udp_udp_stream": "260"},"mndp": {"mndp_mndp_header_unknown": "aa:09","mndp_mndp_header_seqno": "0","mndp_text": ["T 1, L 6: MAC-Address","T 5, L 13: Identity","T 7, L 18: Version","T 8, L 8: Platform","T 10, L 4: Uptime","T 11, L 0: Software-ID","T 12, L 3: Board","T 14, L 1: Unpack","T 15, L 16: IPv6-Address","T 16, L 10: Interface name"],"text_mndp_tlv_type": ["1","5","7","8","10","11","12","14","15","16"],"text_mndp_tlv_length": ["6","13","18","8","4","0","3","1","16","10"],"text_mndp_mac": "00:50:56:9e:05:1b","text_mndp_identity": "fw1.blue07.ex","text_mndp_version": "6.39rc38 (testing)","text_mndp_platform": "MikroTik","text_mndp_uptime": "74231.000000000","text_mndp_board": "CHR","text_mndp_unpack": "1","text_mndp_ipv6address": "2a07:1182:7:1::1","text_mndp_interfacename": "ether5-ops"}}}

 
Post data to the created index

 > curl -H 'Content-Type: application/x-ndjson' -XPOST 'http://192.168.3.31:9200/_bulk?pretty' --data-binary @packets.json

Command to query last 10 logs from created index
 > curl 'http://192.168.3.31:9200/packets-2019-06-20/_search/?size=10&pretty=true'



### Bugs in Setup and Fixes

 Some packages installed using “pip” may throw errors. For instance when installing docker- compose packages using “pip3” In that case upgrade the “pip” to latest version. 

 > sudo pip3 install --upgrade pip

ELK server installation on Log Server VM complains of “limited virtual memory”, the fix is to use “sysctl” command to increase the virtual memory (check stack overflow).



## Data Collection

The data is collected over 5 days. 

### **Monday: Day 1 =>** 

On this day, all the systems in the network have normal behavior, thus contributing to the collection of the normal data set. 

### **Tuesday: Day 2 =>** 

On this day, APT attack is started with the first phase of the cyber kill chain which is Reconnaissance. Reconnaissance mechanisms used are: 
- Network Scanning using NMap
- Application Scanning using BurpSuit and/or WebScarab
- Manual application accounts discovery
- Use of Dirbuster: Performed dirbuster attack on URLs
  - http://206.207.50.35:9000/mutillidae/index.php?page=home.php&popUpNotificationCode=SUD1
  - http://206.207.50.35:9002/
  - http://206.207.50.35:9003/cgi-bin/badstore.cgi 
- Performed nitko scan attack
- Performed sqlmap scanning attack

The above reconnaissance activity can only be seen in the public network traffic, and the private network (192.168.3.30) has no attack traffic on this day.

### **Wednesday: Day 3 =>**

On this day, APT attack progresses to the Establish foothold stage. Involving 
- Downloading and Installing malicious software that gives remote control of the system
- Performed CSRF on 9002
- Performed SQL Injection on 9002
- Php reverse shells. 
- Sending confirmation to C&C

### **Thursday: Day 4 =>** 

On this day, APT attack progresses to the Lateral Movement stage, encompassing 
- SSH from local systems to 192.168.3.30
- Nmap scan on 192.168.3.30
- FTP using weak authentication bypass
- MySQL bypass CVE-2012-2122
- From 192.168.3.29 or Kali Linux run command below
 ```$ for i in `seq 1 1000`; do mysql -uroot -pwrong -h <192.168.3.30> -P3306 ; done```
- DNS Zone Transfer Exploit from “192.168.3.29”
- SMB  CVE-2017-7494, Command shell
- Accessed /etc/passwd file, and got a list of all users
- Accessed /etc/shadow file to see the hashes
- Created a user hacker/aimlsec0718 later updated the password to password
- Created another user invader/hacker and added the user as sudoer, root privileges. 
 - Established the backdoor by uploading shell.php (Ankur’s previous day foothold establishment), and listening on port 4444. Shell obtained from 192.168.3.29. 
- Command Injection performed on 9002 at 9:05 PM


### **Friday: Day 5 =>** 

On this day, APT attack progresses to the Data Exfiltration stage. This includes,
- Command Injection at 6:44 PM 
- Exported the /etc/passwd file to 206.207.50.50 C&C server
- Stopped apache2 service on 9002, and the 9002 site is no longer available. DoS perofrmed
- Use of PyExfil
- Uploaded more files using scp to 206.207.50.50 


## Attack Vectors

The following attack vectors per APT stage are good to have but not all might not be possible in the same setup. Let’s try to have as many as possible. 

**Reconnaissance**

Scan Applications - Nessus, Web Scarab, Burp Suite. Find vulnerabilities such as XSS, XSRF, SQL Injection etc. 
Scan Network - NMap, Portsweep, Mscan, Satan, Ipsweep, Saint. Find systems’ fingerprints, network architecture information etc. Firewall should log deny event. If multiple denies are seen against unique destination ports from the same origin host within a small windows of time, it is safe to assume that some sort of port scanning activity is taking place. 

**Establish Foothold**

Download/Install Malware - Scanbox, Backdoor Sogu, PoisonIvy, KeyLoggers. 
R2L - Guess_Password, Ftp_Write, Imap, Phf, Multihop, Warezmaster, Warezclient, SpyXlock, Xsnoop, Snmpguess, Snmpgetattack, Httptunnel, Sendmail, Named
C&C Communication - Send communication to external server that the malware has been installed. Monitor network traffic originating from a system to an external server, after a download of a file or similar network activitiy.

**Lateral Movement**

Credential Compromise - Key Loggers, Hash retrieval, LDAP, Metasploit
Privilege Escalation (U2R) - Buffer_Overflow, Loadmodule, Rootkit, Perl, Sqlattack, Xterm, PS.

**Internal Reconnaissance** 

Same as Reconnaissance above, just from different source in search of data. IP range might be probed for port 1433 in case of enumerating SQL servers. Ports 135-139 are usually probed by attackers when in search of network shares. 

**Data Exfiltration** 

Uploading to Google Drive, Dropbox, AWS or any such cloud. Need to baseline against the normal activity of a system. 

**Cover Up** 

Deletion of log files, modification of log files etc. Needs host based intrusion detection agent. OUT OF SCOPE for current research.

## Feature Description
There are 76 features that were extracted by the [CICFlowMeter](https://github.com/ahlashkari/CICFlowMeter/blob/master/ReadMe.txt) in addition to 7 fields (1-7 in the table below) that identify a communication flow between 2 systems. Our labeling process has added 2 columns - Activity and Stage, giving users the information on malicious activities and under which stage they fall. All these accumulate into 85 columns in our flow files in the csv folder on this repo. The below table lists the 85 columns in order. 

| Header Column Name | Header Column Description |
| ------ | -------- | 
| Flow ID | Flow Identifier |
| Src IP | Source IP Address |
| Src Port | Source Port |
| Dst IP | Destination IP Address | 
| Dst Port | Destination Port |
| Protocol | Communication Protocol |
| Timestamp | Timestamp of the start of the flow |
| Flow Duration | 	Flow duration  |
| Total Fwd Packet | 	Total packets in the forward direction  |
| Total Bwd packets | 	Total packets in the backward direction  |
| Total Length of Fwd Packet | 	Total size of packet in forward direction  |
| Total Length of Bwd Packet | 	Total size of packet in backward direction  |
| Fwd Packet Length Max | 	Maximum size of packet in forward direction  |
| Fwd Packet Length Min | 	Minimum size of packet in forward direction  |
| Fwd Packet Length Mean | 	Average size of packet in forward direction  |
| Fwd Packet Length Std | 	Standard deviation size of packet in forward direction  |
| Bwd Packet Length Max | 	Maximum size of packet in backward direction  |
| Bwd Packet Length Min | 	Minimum size of packet in backward direction  |
| Bwd Packet Length Mean | 	Mean size of packet in backward direction  |
| Bwd Packet Length Std | 	Standard deviation size of packet in backward direction  |
| Flow Bytes/s | 	flow byte rate that is number of packets transferred per second  |
| Flow Packets/s | 	flow packets rate that is number of packets transferred per second  |
| Flow IAT Mean | 	Average time between two flows    |
| Flow IAT Std | 	Standard deviation time two flows  |
| Flow IAT Max | 	Maximum time between two flows  |
| Flow IAT Min | 	Minimum time between two flows  |
| Fwd IAT Total | 	Total time between two packets sent in the forward direction  |
| Fwd IAT Mean | 	Mean time between two packets sent in the forward direction  |
| Fwd IAT Std | 	Standard deviation time between two packets sent in the forward direction  |
| Fwd IAT Max | 	Maximum time between two packets sent in the forward direction  |
| Fwd IAT Min | 	Minimum time between two packets sent in the forward direction    |
| Bwd IAT Total | 	Total time between two packets sent in the backward direction  |
| Bwd IAT Mean | 	Mean time between two packets sent in the backward direction  |
| Bwd IAT Std | 	Standard deviation time between two packets sent in the backward direction  |
| Bwd IAT Max | 	Maximum time between two packets sent in the backward direction  |
| Bwd IAT Min | 	Minimum time between two packets sent in the backward direction  |
| Fwd PSH Flags | 	Number of times the PSH flag was set in packets travelling in the forward direction (0 for UDP)  |
| Bwd PSH Flags | 	Number of times the PSH flag was set in packets travelling in the backward direction (0 for UDP)  |
| Fwd URG Flags | 	Number of times the URG flag was set in packets travelling in the forward direction (0 for UDP)  |
| Bwd URG Flags | 	Number of times the URG flag was set in packets travelling in the backward direction (0 for UDP)  |
| Fwd Header Length | 	Total bytes used for headers in the forward direction  |
| Bwd Header Length | 	Total bytes used for headers in the forward direction  |
| Fwd Packets/s | 	Number of forward packets per second  |
| Bwd Packets/s | 	Number of backward packets per second  |
| Packet Length Min | 	Minimum length of a flow  |
| Packet Length Max | 	Maximum length of a flow  |
| Packet Length Mean | 	Mean length of a flow  |
| Packet Length Std | 	Standard deviation length of a flow  |
| Packet Length Variance | 	Minimum inter-arrival time of packet  |
| FIN Flag Count | 	Number of packets with FIN  |
| SYN Flag Count | 	Number of packets with SYN  |
| RST Flag Count | 	Number of packets with RST  |
| PSH Flag Count | 	Number of packets with PUSH  |
| ACK Flag Count | 	Number of packets with ACK  |
| URG Flag Count | 	Number of packets with URG  |
| CWR Flag Count | 	Number of packets with CWE  |
| ECE Flag Count | 	Number of packets with ECE  |
| Down/Up Ratio | 	Download and upload ratio  |
| Average Packet Size | 	Average size of packet  |
| Fwd Segment Size Avg | 	Average size observed in the forward direction  |
| Bwd Segment Size Avg | 	Average size observed in the backward direction  |
| Fwd Bytes/Bulk Avg | 	Average number of bytes bulk rate in the forward direction    |
| Fwd Packet/Bulk Avg | 	Average number of packets bulk rate in the forward direction    |
| Fwd Bulk Rate Avg | 	Average number of bulk rate in the forward direction    |
| Bwd Bytes/Bulk Avg | 	Average number of bytes bulk rate in the backward direction    |
| Bwd Packet/Bulk Avg | 	Average number of packets bulk rate in the backward direction    |
| Bwd Bulk Rate Avg | 	Average number of bulk rate in the backward direction    |
| Subflow Fwd Packets | 	The average number of packets in a sub flow in the forward direction    |
| Subflow Fwd Bytes | 	The average number of bytes in a sub flow in the forward direction  |
| Subflow Bwd Packets | 	The average number of packets in a sub flow in the backward direction  |
| Subflow Bwd Bytes | 	The average number of bytes in a sub flow in the backward direction  |
| FWD Init Win Bytes | 	Number of bytes sent in initial window in the forward direction  |
| Bwd Init Win Bytes | 	Number of bytes sent in initial window in the backward direction  |
| Fwd Act Data Pkts | 	Number of packets with at least 1 byte of TCP data payload in the forward direction  |
| Fwd Seg Size Min | 	Minimum segment size observed in the forward direction  |
| Active Mean | 	Mean time a flow was active before becoming idle  |
| Active Std | 	Standard deviation time a flow was active before becoming idle  |
| Active Max | 	Maximum time a flow was active before becoming idle  |
| Active Min | 	Minimum time a flow was active before becoming idle  |
| Idle Mean | 	Mean time a flow was idle before becoming active  |
| Idle Std | 	Standard deviation time a flow was idle before becoming active  |
| Idle Max | 	Maximum time a flow was idle before becoming active  |
| Idle Min | 	Minimum time a flow was idle before becoming active  |
| Activity | Activity this flow represents |
| Stage |  Stages this flow falls under |

## References

[1] https://github.com/vulhub/vulhub  
[2] https://hub.docker.com/r/sebp/elk/  
[3] https://hub.docker.com/r/vulnerables/web-dvwa/  
[4] https://hub.docker.com/r/bltsec/mutillidae-docker/  
[5] https://hub.docker.com/r/jvhoof/badstore-docker/  
[6]https://support.sonatype.com/hc/en-us/articles/360017310793-CVE-2019-7238-Nexus-Repository-Manager-3-Missing-Access-Controls-and-Remote-Code-Execution-February-5th-2019  
[7] https://hub.docker.com/r/vimagick/vsftpd/  
[8] https://www.rapid7.com/db/modules/exploit/unix/ftp/vsftpd_234_backdoor  
[9] https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html  
[10] http://www.netflowmeter.ca/sourcecode.html  
[11] https://www.unb.ca/cic/datasets/ids-2018.html  
[12] https://www.snort.org/  


## How to Commit
git add .
git commit -m "msg"
git push -u origin master
