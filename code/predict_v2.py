# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 06:24:02 2020

@author: Sowmya Myneni
"""
import datagenerator_v2 as datagenerator
import numpy as np
import random
#-------------------------------------------------------------------------------------------------------------------------------------------------------------
#Part 3 - Predicting 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------

from keras.models import load_model
#modelName = "APT_SAELSTM_InitialModel_Monday_CICIDS2017"
modelName = "unb15_model"
#Loads a compiled and saved model
sequence_autoencoder_semi = load_model("/home/ubuntu/aitd/" + modelName + ".h5")
print("Loaded model from disk")


testDatasetFile = input("Please enter the path to the test dataset: \n")
dataset_test = datagenerator.loadDataset(testDatasetFile)

_nSamplesPred = dataset_test.shape[0]
_nColumns = dataset_test.shape[1]
_nTimesteps = 3

X_test = datagenerator.getEncoderInput(dataset_test, 0, _nSamplesPred, _nColumns)
y_test = datagenerator.getEncoderLabelCoulmn(dataset_test, 0, _nSamplesPred-3)

# Feature Scaling -Normalization recommended for RNN    
from sklearn.preprocessing import MinMaxScaler
sc_pred = MinMaxScaler(feature_range = (0, 1))
X_test = sc_pred.fit_transform(X_test)

#Converting prediction inputs into LSTM prediction inputs
_nOperatingColumns = len(X_test[0])
X_test_sequence = datagenerator.getEncoderInputSequence(X_test, _nTimesteps, _nOperatingColumns)
#X_test_sequence_noisy = getEncoderInputSequence(X_test_noisy, _nTimesteps, _nOperatingColumns)

prediction_sequence = sequence_autoencoder_semi.predict(X_test_sequence)

#Removing timesteps in prediction result
prediction_result = []
prediction_input = []
for i in range(len(prediction_sequence)):
    prediction_input.append(X_test_sequence[i, 0, :])
    prediction_result.append(prediction_sequence[i, 0, :])
prediction_input, prediction_result = np.array(prediction_input), np.array(prediction_result)

reconstruction_error = []
for i in range(len(prediction_result)):
    current_record = []
    for j in range(len(prediction_result[0])):
        current_record.append(np.mean(np.power(prediction_input[i, j] - prediction_result[i, j], 2)))
    reconstruction_error.append(np.array(current_record))
outputFileName = "ReconstructionError_" + modelName + ".csv"
np.savetxt(outputFileName, np.array(reconstruction_error), delimiter=',', fmt="%s" )
print("Your file has been saved in thsi same folder under the name : " + outputFileName)

                                      
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import pandas as pd

data = pd.DataFrame(X_test[0:_nSamplesPred-_nTimesteps, :])
data_n = pd.DataFrame(X_test)
data_n = data_n.astype('float32')

dist = np.zeros(_nSamplesPred-_nTimesteps)
for i, x in enumerate(data_n.iloc[0:_nSamplesPred-_nTimesteps, :].values):
    dist[i] = np.linalg.norm(prediction_result[i, :])
    
fpr, tpr, thresholds = roc_curve(y_test, dist)
roc_auc = auc(fpr, tpr)

np.savetxt(modelName+'-fpr.csv', fpr, delimiter="\n")
np.savetxt(modelName+'-tpr.csv', tpr, delimiter="\n")

'''plt.figure()
plt.plot(fpr, tpr, color='red', label='AUC = %0.2f)' % roc_auc)
plt.xlim((0, 1))
plt.ylim((0, 1))
plt.plot([0, 1], [0, 1], color="navy", linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Autoencoder')
plt.legend(loc='lower right')
plt.savefig( testDatasetFile +"ROC-curve_" + "_" + modelName + ".png")
plt.show()
print(testDatasetFile)
'''
